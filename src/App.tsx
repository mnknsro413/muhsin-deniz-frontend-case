import PlayGroundPage from "./pages/play-ground";

function App() {
  return <PlayGroundPage />;
}

export default App;
