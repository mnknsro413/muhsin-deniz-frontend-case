import { useEffect, useState } from "react";
import character from "@/package/asset/image/hero.png";
import trustFloor from "@/package/asset/image/happy-floor.png";
import floor from "@/package/asset/image/floor.png";
import mine from "@/package/asset/image/mine.png";
import genos from "@/package/asset/image/helper-genos.png";
import confetti from "@/package/asset/image/confetti.gif";
import { Cell } from "../types";

const PlayGround: React.FC = () => {
  const rows = 25;
  const columns = 25;
  const totalCells = rows * columns;
  const [totalMine, setTotalMine] = useState<number>(30);
  const [distanceToMine, setDistanceToMine] = useState<number>(0);
  const [points, setPoints] = useState<number>(0);
  const [bestScore, setBestScore] = useState<number>(0);
  const [gameOver, setGameOver] = useState<boolean>(false);
  const [mineOver, setMineOver] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedDifficulty, setSelectedDifficulty] = useState<string>("easy");
  const [showConfetti, setShowConfetti] = useState<boolean>(false);

  useEffect(() => {
    const savedBestScore = parseInt(localStorage.getItem("bestScore") || "0");
    if (!isNaN(savedBestScore)) {
      setBestScore(savedBestScore);
    }
  }, []);

  useEffect(() => {
    if (gameOver) {
      setShowModal(true);
      setShowConfetti(true);
      setTimeout(() => setShowConfetti(false), 2000);
    }
  }, [gameOver]);

  const [area, setArea] = useState<Cell[]>(
    Array.from({ length: totalCells }, () => ({
      hasMine: false,
      isRevealed: false,
      isHero: false,
      isTrustFloor: false,
    }))
  );

  const generateMines = (): Set<number> => {
    const mineLocations = new Set<number>();

    const exitCellIndex = columns * (rows - 1) + Math.floor(columns / 2);
    const excludedCells = getExcludedCells(exitCellIndex, totalCells);
    const excludedArea = getExcludedArea(
      heroPosition.row,
      heroPosition.column,
      columns
    );

    while (mineLocations.size < totalMine) {
      const randomIndex = Math.floor(Math.random() * totalCells);

      if (!excludedCells.has(randomIndex) && !excludedArea.has(randomIndex)) {
        mineLocations.add(randomIndex);
      }
    }

    return mineLocations;
  };

  const getExcludedCells = (
    exitCellIndex: number,
    columns: number
  ): Set<number> => {
    const excludedCells = new Set<number>();
    const excludeDistance = 3; // Karakterin başlangıç pozisyonundan uzaklık

    const exitRow = Math.floor(exitCellIndex / columns);
    const exitColumn = exitCellIndex % columns;

    for (let i = -excludeDistance; i <= excludeDistance; i++) {
      for (let j = -excludeDistance; j <= excludeDistance; j++) {
        const row = exitRow + i;
        const column = exitColumn + j;

        if (row >= 0 && row < rows && column >= 0 && column < columns) {
          excludedCells.add(row * columns + column);
        }
      }
    }

    return excludedCells;
  };

  const getExcludedArea = (
    heroRow: number,
    heroColumn: number,
    columns: number
  ): Set<number> => {
    const excludedArea = new Set<number>();
    const radius = 2;

    for (let i = -radius; i <= radius; i++) {
      for (let j = -radius; j <= radius; j++) {
        const row = heroRow + i;
        const column = heroColumn + j;

        if (row >= 0 && row < rows && column >= 0 && column < columns) {
          excludedArea.add(row * columns + column);
        }
      }
    }

    return excludedArea;
  };
  const [heroPosition, setHeroPosition] = useState<{
    row: number;
    column: number;
  }>({
    row: 0,
    column: 0,
  });
  const [mineLocations, setMineLocations] = useState<Set<number>>(
    generateMines()
  );

  useEffect(() => {
    const handleKeyPress = (event: KeyboardEvent) => {
      if (gameOver) return;

      const keyCode = event.keyCode;
      let newRow = heroPosition.row;
      let newColumn = heroPosition.column;
      switch (keyCode) {
        case 37: // sol
          newColumn = newColumn > 0 ? newColumn - 1 : newColumn;
          break;
        case 38: // yukarı
          newRow = newRow > 0 ? newRow - 1 : newRow;
          break;
        case 39: // sağ
          newColumn = newColumn < columns - 1 ? newColumn + 1 : newColumn;
          break;
        case 40: // aşağı
          newRow = newRow < rows - 1 ? newRow + 1 : newRow;
          break;
        default:
          break;
      }

      if (mineLocations.has(newRow * columns + newColumn)) {
        setShowModal(true);
        setGameOver(true);
        setShowConfetti(false);
        setMineOver(false);
        return;
      }

      if (newRow === rows - 1) {
        setShowModal(true);
        setMineOver(true);
        setShowConfetti(true);
        setTimeout(() => setShowConfetti(false), 2000);
        return;
      }

      setDistanceToMine(calculateDistanceToMine(newRow, newColumn));
      setPoints((prevPoints) => prevPoints + 1);
      setHeroPosition({ row: newRow, column: newColumn });
    };

    document.addEventListener("keydown", handleKeyPress);

    return () => {
      document.removeEventListener("keydown", handleKeyPress);
    };
  }, [heroPosition, columns, rows, mineLocations, gameOver, points]);

  useEffect(() => {
    if (gameOver) {
      if (heroPosition.row === rows - 1) {
        setShowModal(true);
        setShowConfetti(true);
        setTimeout(() => setShowConfetti(false), 2000);
      } else {
        setShowModal(true);
        setShowConfetti(false);
      }
    }
  }, [gameOver, heroPosition, rows]);

  const calculateDistanceToMine = (row: number, column: number): number => {
    const distances: number[] = [];
    mineLocations.forEach((mineIndex) => {
      const mineRow = Math.floor(mineIndex / columns);
      const mineColumn = mineIndex % columns;
      const distance = Math.abs(row - mineRow) + Math.abs(column - mineColumn);
      distances.push(distance);
    });
    return Math.min(...distances);
  };

  const resetGame = (): void => {
    setShowModal(false);
    setGameOver(false);
    setPoints(0);
    setDistanceToMine(0);
    setHeroPosition({ row: 0, column: 0 });
    setMineLocations(generateMines());
    setArea(
      Array.from({ length: totalCells }, () => ({
        hasMine: false,
        isRevealed: false,
        isHero: false,
        isTrustFloor: false,
      }))
    );
  };

  const handleDifficultyChange = (
    e: React.ChangeEvent<HTMLSelectElement>
  ): void => {
    setSelectedDifficulty(e.target.value);
    const difficulty = e.target.value;

    if (difficulty === "easy") {
      setTotalMine(20);
    } else if (difficulty === "medium") {
      setTotalMine(70);
    } else if (difficulty === "hard") {
      setTotalMine(120);
    }

    const newMineLocations = generateMines();
    setMineLocations(newMineLocations);
  };

  const renderArea = (): JSX.Element => {
    return (
      <div className="grid grid-cols-25 gap-1">
        {area.map((cell, index) => {
          const row = Math.floor(index / columns);
          const column = index % columns;

          return (
            <div key={index} className={`h-13 w-10 border`}>
              {row === heroPosition.row && column === heroPosition.column ? (
                <img
                  src={character}
                  alt="Character"
                  className="object-cover rounded-full w-10 h-10"
                />
              ) : cell.isRevealed ? (
                mineLocations.has(index) ? (
                  <img
                    src={mine}
                    alt="Mine"
                    className="object-cover w-10 h-10"
                  />
                ) : (
                  <img
                    src={trustFloor}
                    alt="TrustFloor"
                    className="object-cover w-10 h-10"
                  />
                )
              ) : gameOver ? (
                mineLocations.has(index) ? (
                  <img
                    src={mine}
                    alt="Mine"
                    className="object-cover w-10 h-10"
                  />
                ) : (
                  <img
                    src={trustFloor}
                    alt="TrustFloor"
                    className="object-cover w-10 h-10"
                  />
                )
              ) : (
                <img
                  src={floor}
                  alt="Floor"
                  className="object-cover w-10 h-10"
                />
              )}
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div className="flex justify-center items-center h-screen relative">
      {mineOver && showConfetti && (
        <img
          src={confetti}
          alt="Confetti"
          className="fixed inset-0 z-50 left-1/2 top-1/3 -translate-x-1/2"
        />
      )}
      <div className="absolute flex items-center left-3 top-4 p-3">
        Point: {points} - Best Score: {bestScore}
      </div>

      <div className="absolute flex items-center right-3 top-4 p-3">
        {distanceToMine > 0 && (
          <div className="flex flex-col w-full max-w-[326px] leading-1.5 p-3 border-gray-200 bg-gray-100 rounded-s-xl rounded-ee-xl dark:bg-gray-700 text-white">
            {`Distance to nearest enemy: ${distanceToMine}`}
          </div>
        )}

        <div className="relative">
          <img src={genos} alt="Genos" className="w-[120px]" />
        </div>
      </div>

      {renderArea()}
      {showModal && (
        <div className="absolute inset-0 flex justify-center items-center bg-gray-900 bg-opacity-70">
          <div className="bg-white p-4 rounded-md text-center">
            <h2 className="text-2xl font-bold">
              {gameOver && "Üzgünüz!"}
              {mineOver && "Başarılı!"}
            </h2>
            <p>
              {gameOver && "Üzgünüz, tekrar deneyiniz."}

              {mineOver && "Oyunu başarıyla tamamladınız!"}
            </p>
            <p>
              <button
                onClick={resetGame}
                className="bg-blue-500 text-white px-4 py-2 rounded-md mt-4"
              >
                Tekrar Oyna
              </button>
            </p>
            <div>
              Zorluk seviyesi:
              <select
                value={selectedDifficulty}
                onChange={handleDifficultyChange}
                className="p-2 rounded-md"
              >
                <option value="easy">Basit</option>
                <option value="medium">Orta</option>
                <option value="hard">Zor</option>
              </select>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default PlayGround;
