import PlayGround from "../../module/play-ground/view/play-ground";

const PlayGroundPage = () => {
  return <PlayGround />;
};

export default PlayGroundPage;
