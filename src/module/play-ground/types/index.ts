export interface Cell {
    hasMine: boolean;
    isRevealed: boolean;
    isTrustFloor: boolean;
}