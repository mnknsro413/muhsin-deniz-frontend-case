import react from "@vitejs/plugin-react"
import * as path from "path"
import { defineConfig, loadEnv } from "vite"

// https://vitejs.dev/config/
export default defineConfig(async ({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd(), "") }

  return {
    plugins: [
      react(),
    ],
    resolve: {
      alias: [{ find: "@", replacement: path.resolve(__dirname, "src") }],
    },
  }
})
